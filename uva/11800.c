/* Problem URL: https://uva.onlinejudge.org/external/118/11800.pdf */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <errno.h>
#include <string.h>

#define STR(X) #X
#define XSTR(X) STR(X)
#define __LOC__ __FILE__ ":" XSTR (__LINE__)
#define __LOC_PRE__ __LOC__ ": "

#define LENGTH(arr) (sizeof (arr) / sizeof (arr[0]))

struct vector
{
  int x, y;
};

typedef struct vector vector_t;

struct ql
{
  vector_t p[4];
};

typedef struct ql ql_t;

int anglecmp (const void *const a, const void *const b);
int vectorcmp (const void *const a, const void *const b);
bool issquare (const ql_t * const q);
bool isrect (const ql_t * const q);
bool isrhomb (const ql_t * const q);
bool isparall (const ql_t * const q);
bool istrap (const ql_t * const q);

/* midpoint */
double mx, my;

int
main (void)
{
  unsigned ncases;
  if (scanf ("%u", &ncases) != 1)
    {
      perror (__LOC__);
      exit (EXIT_FAILURE);
    }

  for (unsigned i = 0; i < ncases; ++i)
    {
      ql_t q;
      mx = my = 0;
      for (size_t j = 0; j < LENGTH (q.p); ++j)
	{
	  if (scanf ("%d %d", &q.p[j].x, &q.p[j].y) != 2)
	    {
	      perror (__LOC__);
	      exit (EXIT_FAILURE);
	    }

	  mx += q.p[j].x;
	  my += q.p[j].y;
	}

      mx /= LENGTH (q.p);
      my /= LENGTH (q.p);

      qsort (&q.p[0], LENGTH (q.p), sizeof (q.p[0]), anglecmp);

      const char *shape = NULL;
      if (issquare (&q))
	shape = "Square";
      else if (isrect (&q))
	shape = "Rectangle";
      else if (isrhomb (&q))
	shape = "Rhombus";
      else if (isparall (&q))
	shape = "Parallelogram";
      else if (istrap (&q))
	shape = "Trapezium";
      else
	shape = "Ordinary Quadrilateral";

      if (printf ("Case %u: %s\n", i + 1, shape) < 0)
	{
	  perror (__LOC__);
	  exit (EXIT_FAILURE);

	}
    }

  exit (EXIT_SUCCESS);
}

static inline bool
is_parallel (const vector_t v1, const vector_t v2)
{
  return v1.x * v2.y - v1.y * v2.x == 0;
}

static inline bool
is_perpendicular (const vector_t v1, const vector_t v2)
{
  return v1.x * v2.x + v1.y * v2.y == 0;
}

/* magnitude squared */
static inline unsigned
mag2 (const vector_t v)
{
  return v.x * v.x + v.y * v.y;
}

/* vector from point a to b */
static inline vector_t
from_points (const vector_t a, const vector_t b)
{
  vector_t ret;
  ret.x = b.x - a.x;
  ret.y = b.y - a.y;

  return ret;
}

bool
issquare (const ql_t * const q)
{
  return isrect (q) && isrhomb (q);
}

bool
isrect (const ql_t * const q)
{
  const vector_t v1 = from_points (q->p[0], q->p[1]);
  const vector_t v2 = from_points (q->p[1], q->p[2]);

  return isparall (q) && is_perpendicular (v1, v2);
}

bool
isrhomb (const ql_t * const q)
{
  const vector_t v1 = from_points (q->p[0], q->p[1]);
  const vector_t v2 = from_points (q->p[1], q->p[2]);
  const vector_t v3 = from_points (q->p[2], q->p[3]);
  const vector_t v4 = from_points (q->p[3], q->p[0]);

  return isparall (q)
    && mag2 (v1) == mag2 (v2)
    && mag2 (v2) == mag2 (v3) && mag2 (v3) == mag2 (v4);
}

bool
isparall (const ql_t * const q)
{
  const vector_t v1 = from_points (q->p[0], q->p[1]);
  const vector_t v2 = from_points (q->p[1], q->p[2]);
  const vector_t v3 = from_points (q->p[2], q->p[3]);
  const vector_t v4 = from_points (q->p[3], q->p[0]);

  return is_parallel (v1, v3) && is_parallel (v2, v4);
}

bool
istrap (const ql_t * q)
{
  const vector_t v1 = from_points (q->p[0], q->p[1]);
  const vector_t v2 = from_points (q->p[1], q->p[2]);
  const vector_t v3 = from_points (q->p[2], q->p[3]);
  const vector_t v4 = from_points (q->p[3], q->p[0]);

  return is_parallel (v1, v3) ^ is_parallel (v2, v4);
}

static inline int
icmp (const int i1, const int i2)
{
  return (i1 < i2) ? -1 : (i1 == i2) ? 0 : 1;
}

int
vectorcmp (const void *const a, const void *const b)
{
  const vector_t *const p1 = (vector_t * const) a;
  const vector_t *const p2 = (vector_t * const) b;

  return (icmp (p1->x, p2->x) != 0) ? icmp (p1->x, p2->x) : icmp (p1->y,
								  p2->y);
}

int
anglecmp (const void *const a, const void *const b)
{
  const vector_t *const p1 = (vector_t * const) a;
  const vector_t *const p2 = (vector_t * const) b;

  const double d1x = p1->x - mx;
  const double d1y = p1->y - my;
  const double d2x = p2->x - mx;
  const double d2y = p2->y - my;

  const double theta1 = atan2 (d1y, d1x);
  const double theta2 = atan2 (d2y, d2x);

  return (theta1 < theta2) ? -1 : ((theta1 == theta2) ? 0 : 1);
}
