/* Problem URL: https://icpcarchive.ecs.baylor.edu/external/39/3996.pdf */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <errno.h>
#include <string.h>

#define STR(X) #X
#define XSTR(X) STR(X)
#define __LOC__ __FILE__ ":" XSTR (__LINE__)
#define __LOC_PRE__ __LOC__ ": "

#define LENGTH(arr) (sizeof (arr) / sizeof (arr[0]))

#define BUFSIZE 16

int
main (void)
{
  unsigned ncases;

  if (scanf ("%u", &ncases) != 1)
    {
      perror (__LOC__);
      exit (EXIT_FAILURE);
    }


  for (unsigned i = 0; i < ncases; ++i)
    {
      unsigned n;

      if (scanf ("%u", &n) != 1)
	{
	  perror (__LOC__);
	  exit (EXIT_FAILURE);
	}

      unsigned dcount['9' - '0' + 1];
      memset (dcount, 0, sizeof (dcount));

      char buf[BUFSIZE];
      for (unsigned j = 1; j <= n; ++j)
	{
	  sprintf (buf, "%u", j);
	  const size_t blen = strlen (buf);
	  for (size_t k = 0; k < blen; ++k)
	    ++dcount[buf[k] - '0'];
	}

      if (printf ("%u", dcount[0]) < 0)
	{
	  perror (__LOC__);
	  exit (EXIT_FAILURE);
	}

      for (size_t j = 1; j < LENGTH (dcount); ++j)
	if (printf (" %u", dcount[j]) < 0)
	  {
	    perror (__LOC__);
	    exit (EXIT_FAILURE);
	  }

      if (putchar ('\n') < 0)
	{
	  perror (__LOC__);
	  exit (EXIT_FAILURE);
	}
    }

  exit (EXIT_SUCCESS);
}
