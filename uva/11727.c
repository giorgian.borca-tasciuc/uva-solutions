/* Problem URL: https://uva.onlinejudge.org/external/117/11727.pdf */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#define STR(X) #X
#define XSTR(X) STR(X)
#define __LOC__ __FILE__ ":" XSTR (__LINE__)
#define __LOC_PRE__ __LOC__ ": "

#define LENGTH(arr) (sizeof (arr) / sizeof (arr[0]))

#define NEMPS 3
#define SEMP 1

int uicmp (const void *const p1, const void *const p2);

int
main (void)
{
  unsigned ncases;

  if (scanf ("%u", &ncases) != 1)
    {
      perror (__LOC__);
      exit (EXIT_FAILURE);
    }

  for (unsigned i = 0; i < ncases; ++i)
    {
      unsigned emps[NEMPS];
      for (size_t j = 0; j < LENGTH (emps); ++j)
	if (scanf ("%u", &emps[j]) != 1)
	  {
	    perror (__LOC__);
	    exit (EXIT_FAILURE);
	  }

      qsort (emps, LENGTH (emps), sizeof *emps, uicmp);
      if (printf ("Case %u: %u\n", (i + 1), emps[SEMP]) < 0)
	{
	  perror (__LOC__);
	  exit (EXIT_FAILURE);
	}
    }

  exit (EXIT_SUCCESS);
}

int
uicmp (const void *const p1, const void *const p2)
{
  const unsigned *const u1 = (const unsigned *const) p1;
  const unsigned *const u2 = (const unsigned *const) p2;

  return (*u1 < *u2) ? -1 : (*u1 == *u2) ? 0 : 1;
}
